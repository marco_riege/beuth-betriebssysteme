if [ ! -d ./backup ]
then
  mkdir ./backup/
fi

for i in *$1
do
  #!/bin/sh +x aufgabe1.sh
  ./aufgabe1.sh  "Soll von der $i ein Backup erstellt werden?" "ja" "nein"		
  if [ $? -eq 0 ]
  then
    cp $i ./backup/
  fi
done
