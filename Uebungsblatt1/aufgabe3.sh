size=$#
while [ $size -gt 0 ]
do
  count=1
  for para in $@
  do
    if [ $count -eq $size ]
    then
      echo "Nr: $size = $para"
      size=$(expr $size - 1)
    fi
    count=$(expr $count + 1)
  done
done
