import os
import pickle

dict = {}

# get files
all_files = os.listdir("test/")
txt_files = filter(lambda x: x[-4:] == '.txt', all_files)

# read words of file
for file in txt_files:
  f = open('test/'+file, 'r')
  for line in f:
    for word in line.split():
      # check if word is already in dictionary
      if word not in dict:
        dict[word] = [file]
      else:
        if file not in dict[word]:
          dict[word].append(file)

# save dict with pickle module
pickle_out = open("dict.pickle","wb")
pickle.dump(dict, pickle_out)
pickle_out.close()
